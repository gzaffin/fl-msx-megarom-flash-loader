# FL MSX MEGAROM FLASH LOADER

## References links

[gdx's FL (MSXvillage.fr)](http://www.msxvillage.fr/upload/fl_v132.zip) is code base starting point. Basically major effort was made by gdx.

[ESE Artists' Factory pseudoROM page](http://www.hat.hi-ho.ne.jp/tujikawa/flashrom/flashrom.html) has [FLLOAD](http://www.hat.hi-ho.ne.jp/tujikawa/flashrom/flload.zip), that is ESE Artists' Factory/K.Tsujikawa originator project.

## How to build

### GNU/Linux bash
> ``$ ~/pasmo-0.5.4.beta2/pasmo --bin FL8.asm FL8.COM FL8.SYM``

> ``$ ~/pasmo-0.5.4.beta2/pasmo --bin FL16.asm FL16.COM FL16.SYM``


### Windows command line or Windows PowerShell
> ``>.\pasmo-0.5.4.beta2\pasmo.exe --bin FL8.asm FL8.COM FL8.SYM``

> ``>.\pasmo-0.5.4.beta2\pasmo.exe --bin FL16.asm FL16.COM FL16.SYM``

# License

This code is available open source under the terms of the [GNU General Public License version 2](https://opensource.org/licenses/GPL-2.0).
